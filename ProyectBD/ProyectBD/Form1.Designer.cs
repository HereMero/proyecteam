﻿namespace ProyectBD
{
	partial class Form1
	{
		/// <summary>
		/// Variable del diseñador necesaria.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Limpiar los recursos que se estén usando.
		/// </summary>
		/// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Código generado por el Diseñador de Windows Forms

		/// <summary>
		/// Método necesario para admitir el Diseñador. No se puede modificar
		/// el contenido de este método con el editor de código.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
			this.TxUsuario = new System.Windows.Forms.TextBox();
			this.TxContraseña = new System.Windows.Forms.TextBox();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.BTNEntrar = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SuspendLayout();
			// 
			// TxUsuario
			// 
			this.TxUsuario.BackColor = System.Drawing.Color.Gray;
			this.TxUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.TxUsuario.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.TxUsuario.ForeColor = System.Drawing.SystemColors.ControlLightLight;
			this.TxUsuario.Location = new System.Drawing.Point(28, 150);
			this.TxUsuario.Name = "TxUsuario";
			this.TxUsuario.Size = new System.Drawing.Size(255, 20);
			this.TxUsuario.TabIndex = 4;
			this.TxUsuario.Text = "Usuario";
			this.TxUsuario.TextChanged += new System.EventHandler(this.TxUsuario_TextChanged);
			// 
			// TxContraseña
			// 
			this.TxContraseña.BackColor = System.Drawing.Color.Gray;
			this.TxContraseña.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.TxContraseña.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.TxContraseña.ForeColor = System.Drawing.SystemColors.ControlLightLight;
			this.TxContraseña.Location = new System.Drawing.Point(28, 210);
			this.TxContraseña.Name = "TxContraseña";
			this.TxContraseña.Size = new System.Drawing.Size(255, 20);
			this.TxContraseña.TabIndex = 5;
			this.TxContraseña.Text = "Contraseña";
			// 
			// pictureBox1
			// 
			this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
			this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
			this.pictureBox1.Location = new System.Drawing.Point(115, 12);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(89, 101);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.pictureBox1.TabIndex = 6;
			this.pictureBox1.TabStop = false;
			// 
			// BTNEntrar
			// 
			this.BTNEntrar.Location = new System.Drawing.Point(28, 253);
			this.BTNEntrar.Name = "BTNEntrar";
			this.BTNEntrar.Size = new System.Drawing.Size(75, 23);
			this.BTNEntrar.TabIndex = 7;
			this.BTNEntrar.Text = "Entrar";
			this.BTNEntrar.UseVisualStyleBackColor = true;
			this.BTNEntrar.Click += new System.EventHandler(this.BTNEntrar_Click);
			// 
			// button1
			// 
			this.button1.BackColor = System.Drawing.Color.Maroon;
			this.button1.FlatAppearance.BorderSize = 5;
			this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.button1.Location = new System.Drawing.Point(262, 12);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(41, 31);
			this.button1.TabIndex = 8;
			this.button1.Text = "X";
			this.button1.UseVisualStyleBackColor = false;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.Black;
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(315, 334);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.BTNEntrar);
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.TxContraseña);
			this.Controls.Add(this.TxUsuario);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.MinimizeBox = false;
			this.Name = "Form1";
			this.Opacity = 0.9D;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Login";
			this.Load += new System.EventHandler(this.Form1_Load);
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion
		private System.Windows.Forms.TextBox TxUsuario;
		private System.Windows.Forms.TextBox TxContraseña;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.Button BTNEntrar;
		private System.Windows.Forms.Button button1;
	}
}

