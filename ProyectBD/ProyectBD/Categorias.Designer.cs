﻿namespace ProyectBD
{
	partial class Categorias
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Categorias));
			this.toolStrip1 = new System.Windows.Forms.ToolStrip();
			this.btnNuevo = new System.Windows.Forms.ToolStripButton();
			this.btnGuardar = new System.Windows.Forms.ToolStripButton();
			this.btnEliminar = new System.Windows.Forms.ToolStripButton();
			this.btnModificar = new System.Windows.Forms.ToolStripButton();
			this.panel1 = new System.Windows.Forms.Panel();
			this.TxTipo = new System.Windows.Forms.TextBox();
			this.Tipo = new System.Windows.Forms.Label();
			this.TxDescripcion = new System.Windows.Forms.TextBox();
			this.Descripcion = new System.Windows.Forms.Label();
			this.TxClave = new System.Windows.Forms.TextBox();
			this.Clave = new System.Windows.Forms.Label();
			this.Grid = new System.Windows.Forms.DataGridView();
			this.proyect1DataSet = new ProyectBD.Proyect1DataSet();
			this.categoriaBindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.categoriaTableAdapter = new ProyectBD.Proyect1DataSetTableAdapters.CategoriaTableAdapter();
			this.catClaveDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.catDescripciónDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.catTipoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.toolStrip1.SuspendLayout();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.proyect1DataSet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.categoriaBindingSource)).BeginInit();
			this.SuspendLayout();
			// 
			// toolStrip1
			// 
			this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnNuevo,
            this.btnGuardar,
            this.btnEliminar,
            this.btnModificar});
			this.toolStrip1.Location = new System.Drawing.Point(0, 0);
			this.toolStrip1.Name = "toolStrip1";
			this.toolStrip1.Size = new System.Drawing.Size(414, 25);
			this.toolStrip1.TabIndex = 1;
			this.toolStrip1.Text = "toolStrip1";
			// 
			// btnNuevo
			// 
			this.btnNuevo.Image = ((System.Drawing.Image)(resources.GetObject("btnNuevo.Image")));
			this.btnNuevo.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnNuevo.Name = "btnNuevo";
			this.btnNuevo.Size = new System.Drawing.Size(62, 22);
			this.btnNuevo.Text = "Nuevo";
			this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
			// 
			// btnGuardar
			// 
			this.btnGuardar.Image = ((System.Drawing.Image)(resources.GetObject("btnGuardar.Image")));
			this.btnGuardar.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnGuardar.Name = "btnGuardar";
			this.btnGuardar.Size = new System.Drawing.Size(69, 22);
			this.btnGuardar.Text = "&Guardar";
			this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
			// 
			// btnEliminar
			// 
			this.btnEliminar.Image = ((System.Drawing.Image)(resources.GetObject("btnEliminar.Image")));
			this.btnEliminar.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnEliminar.Name = "btnEliminar";
			this.btnEliminar.Size = new System.Drawing.Size(70, 22);
			this.btnEliminar.Text = "&Eliminar";
			this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
			// 
			// btnModificar
			// 
			this.btnModificar.Image = ((System.Drawing.Image)(resources.GetObject("btnModificar.Image")));
			this.btnModificar.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnModificar.Name = "btnModificar";
			this.btnModificar.Size = new System.Drawing.Size(78, 22);
			this.btnModificar.Text = "Modificar";
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.LightCoral;
			this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panel1.Controls.Add(this.TxTipo);
			this.panel1.Controls.Add(this.Tipo);
			this.panel1.Controls.Add(this.TxDescripcion);
			this.panel1.Controls.Add(this.Descripcion);
			this.panel1.Controls.Add(this.TxClave);
			this.panel1.Controls.Add(this.Clave);
			this.panel1.Location = new System.Drawing.Point(0, 28);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(414, 184);
			this.panel1.TabIndex = 2;
			// 
			// TxTipo
			// 
			this.TxTipo.Location = new System.Drawing.Point(117, 105);
			this.TxTipo.Name = "TxTipo";
			this.TxTipo.Size = new System.Drawing.Size(161, 20);
			this.TxTipo.TabIndex = 5;
			// 
			// Tipo
			// 
			this.Tipo.AutoSize = true;
			this.Tipo.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Tipo.Location = new System.Drawing.Point(44, 105);
			this.Tipo.Name = "Tipo";
			this.Tipo.Size = new System.Drawing.Size(34, 15);
			this.Tipo.TabIndex = 4;
			this.Tipo.Text = "Tipo";
			// 
			// TxDescripcion
			// 
			this.TxDescripcion.Location = new System.Drawing.Point(117, 36);
			this.TxDescripcion.Multiline = true;
			this.TxDescripcion.Name = "TxDescripcion";
			this.TxDescripcion.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.TxDescripcion.Size = new System.Drawing.Size(248, 63);
			this.TxDescripcion.TabIndex = 3;
			// 
			// Descripcion
			// 
			this.Descripcion.AutoSize = true;
			this.Descripcion.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Descripcion.Location = new System.Drawing.Point(38, 39);
			this.Descripcion.Name = "Descripcion";
			this.Descripcion.Size = new System.Drawing.Size(78, 15);
			this.Descripcion.TabIndex = 2;
			this.Descripcion.Text = "Descripcion";
			// 
			// TxClave
			// 
			this.TxClave.Location = new System.Drawing.Point(117, 5);
			this.TxClave.Name = "TxClave";
			this.TxClave.Size = new System.Drawing.Size(161, 20);
			this.TxClave.TabIndex = 1;
			// 
			// Clave
			// 
			this.Clave.AutoSize = true;
			this.Clave.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Clave.Location = new System.Drawing.Point(44, 7);
			this.Clave.Name = "Clave";
			this.Clave.Size = new System.Drawing.Size(39, 15);
			this.Clave.TabIndex = 0;
			this.Clave.Text = "Clave";
			// 
			// Grid
			// 
			this.Grid.AllowUserToAddRows = false;
			this.Grid.AllowUserToDeleteRows = false;
			this.Grid.AllowUserToResizeColumns = false;
			this.Grid.AutoGenerateColumns = false;
			this.Grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.Grid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.catClaveDataGridViewTextBoxColumn,
            this.catDescripciónDataGridViewTextBoxColumn,
            this.catTipoDataGridViewTextBoxColumn});
			this.Grid.DataSource = this.categoriaBindingSource;
			this.Grid.Location = new System.Drawing.Point(0, 230);
			this.Grid.MultiSelect = false;
			this.Grid.Name = "Grid";
			this.Grid.ReadOnly = true;
			this.Grid.RowHeadersVisible = false;
			this.Grid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.Grid.Size = new System.Drawing.Size(414, 150);
			this.Grid.TabIndex = 3;
			this.Grid.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Grid_CellClick);
			this.Grid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Grid_CellContentClick);
			// 
			// proyect1DataSet
			// 
			this.proyect1DataSet.DataSetName = "Proyect1DataSet";
			this.proyect1DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
			// 
			// categoriaBindingSource
			// 
			this.categoriaBindingSource.DataMember = "Categoria";
			this.categoriaBindingSource.DataSource = this.proyect1DataSet;
			// 
			// categoriaTableAdapter
			// 
			this.categoriaTableAdapter.ClearBeforeFill = true;
			// 
			// catClaveDataGridViewTextBoxColumn
			// 
			this.catClaveDataGridViewTextBoxColumn.DataPropertyName = "Cat_Clave";
			this.catClaveDataGridViewTextBoxColumn.HeaderText = "Clave";
			this.catClaveDataGridViewTextBoxColumn.Name = "catClaveDataGridViewTextBoxColumn";
			this.catClaveDataGridViewTextBoxColumn.ReadOnly = true;
			// 
			// catDescripciónDataGridViewTextBoxColumn
			// 
			this.catDescripciónDataGridViewTextBoxColumn.DataPropertyName = "Cat_Descripción";
			this.catDescripciónDataGridViewTextBoxColumn.HeaderText = "Descripción";
			this.catDescripciónDataGridViewTextBoxColumn.Name = "catDescripciónDataGridViewTextBoxColumn";
			this.catDescripciónDataGridViewTextBoxColumn.ReadOnly = true;
			// 
			// catTipoDataGridViewTextBoxColumn
			// 
			this.catTipoDataGridViewTextBoxColumn.DataPropertyName = "Cat_Tipo";
			this.catTipoDataGridViewTextBoxColumn.HeaderText = "Tipo";
			this.catTipoDataGridViewTextBoxColumn.Name = "catTipoDataGridViewTextBoxColumn";
			this.catTipoDataGridViewTextBoxColumn.ReadOnly = true;
			// 
			// Categorias
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.Highlight;
			this.ClientSize = new System.Drawing.Size(414, 408);
			this.Controls.Add(this.Grid);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.toolStrip1);
			this.Name = "Categorias";
			this.Text = "Categoria";
			this.Load += new System.EventHandler(this.Categorias_Load);
			this.toolStrip1.ResumeLayout(false);
			this.toolStrip1.PerformLayout();
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.proyect1DataSet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.categoriaBindingSource)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ToolStrip toolStrip1;
		private System.Windows.Forms.ToolStripButton btnNuevo;
		private System.Windows.Forms.ToolStripButton btnGuardar;
		private System.Windows.Forms.ToolStripButton btnEliminar;
		private System.Windows.Forms.ToolStripButton btnModificar;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.TextBox TxTipo;
		private System.Windows.Forms.Label Tipo;
		private System.Windows.Forms.TextBox TxDescripcion;
		private System.Windows.Forms.Label Descripcion;
		private System.Windows.Forms.TextBox TxClave;
		private System.Windows.Forms.Label Clave;
		private System.Windows.Forms.DataGridView Grid;
		private Proyect1DataSet proyect1DataSet;
		private System.Windows.Forms.BindingSource categoriaBindingSource;
		private Proyect1DataSetTableAdapters.CategoriaTableAdapter categoriaTableAdapter;
		private System.Windows.Forms.DataGridViewTextBoxColumn catClaveDataGridViewTextBoxColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn catDescripciónDataGridViewTextBoxColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn catTipoDataGridViewTextBoxColumn;
	}
}