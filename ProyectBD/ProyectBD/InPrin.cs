﻿using ProyectBD.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectBD
{
	public partial class InPrin : Form
	{
		public InPrin()
		{
			InitializeComponent();
		}
		Categorias categorias = new Categorias();
		Model.Usuarios usuarios = new Model.Usuarios();
		private void Cerrar_Click(object sender, EventArgs e)
		{
			Form1 form = new Form1();

			form.Show();
			this.Hide();

		}

		bool RolG;
		public void Rol(bool rol)
		{
			RolG = rol;
		}



		private void BtCategoria_Click(object sender, EventArgs e)
		{
	

			if (RolG == false)
			{
				MessageBox.Show("NO tienes permitido entrar");
			}
			else
			{
				categorias.Show();
			}
		}

		private void AdminUser_Click(object sender, EventArgs e)
		{
			if (RolG == false)
			{
				MessageBox.Show("NO tienes permitido entrar");
			}
			else
			{
				
			}
		}
	}
}
