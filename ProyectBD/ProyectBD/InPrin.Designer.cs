﻿namespace ProyectBD
{
	partial class InPrin
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.AdminUser = new System.Windows.Forms.Button();
			this.BtClientes = new System.Windows.Forms.Button();
			this.BtCategoria = new System.Windows.Forms.Button();
			this.Cerrar = new System.Windows.Forms.Button();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.Brown;
			this.panel1.Controls.Add(this.Cerrar);
			this.panel1.Controls.Add(this.AdminUser);
			this.panel1.Controls.Add(this.BtClientes);
			this.panel1.Controls.Add(this.BtCategoria);
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(212, 288);
			this.panel1.TabIndex = 0;
			// 
			// AdminUser
			// 
			this.AdminUser.FlatAppearance.BorderColor = System.Drawing.Color.Lime;
			this.AdminUser.FlatAppearance.BorderSize = 23;
			this.AdminUser.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.AdminUser.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.AdminUser.Location = new System.Drawing.Point(0, 131);
			this.AdminUser.Name = "AdminUser";
			this.AdminUser.Size = new System.Drawing.Size(212, 38);
			this.AdminUser.TabIndex = 3;
			this.AdminUser.Text = "Usuarios";
			this.AdminUser.UseVisualStyleBackColor = true;
			this.AdminUser.Click += new System.EventHandler(this.AdminUser_Click);
			// 
			// BtClientes
			// 
			this.BtClientes.FlatAppearance.BorderColor = System.Drawing.Color.Lime;
			this.BtClientes.FlatAppearance.BorderSize = 23;
			this.BtClientes.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.BtClientes.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.BtClientes.Location = new System.Drawing.Point(0, 76);
			this.BtClientes.Name = "BtClientes";
			this.BtClientes.Size = new System.Drawing.Size(212, 38);
			this.BtClientes.TabIndex = 2;
			this.BtClientes.Text = "Clientes";
			this.BtClientes.UseVisualStyleBackColor = true;
			// 
			// BtCategoria
			// 
			this.BtCategoria.FlatAppearance.BorderColor = System.Drawing.Color.Lime;
			this.BtCategoria.FlatAppearance.BorderSize = 23;
			this.BtCategoria.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.BtCategoria.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.BtCategoria.Location = new System.Drawing.Point(0, 21);
			this.BtCategoria.Name = "BtCategoria";
			this.BtCategoria.Size = new System.Drawing.Size(212, 38);
			this.BtCategoria.TabIndex = 1;
			this.BtCategoria.Text = "Categoria";
			this.BtCategoria.UseVisualStyleBackColor = true;
			this.BtCategoria.Click += new System.EventHandler(this.BtCategoria_Click);
			// 
			// Cerrar
			// 
			this.Cerrar.FlatAppearance.BorderColor = System.Drawing.Color.Lime;
			this.Cerrar.FlatAppearance.BorderSize = 23;
			this.Cerrar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.Cerrar.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.Cerrar.Location = new System.Drawing.Point(0, 230);
			this.Cerrar.Name = "Cerrar";
			this.Cerrar.Size = new System.Drawing.Size(212, 38);
			this.Cerrar.TabIndex = 4;
			this.Cerrar.Text = "Cerrar";
			this.Cerrar.UseVisualStyleBackColor = true;
			this.Cerrar.Click += new System.EventHandler(this.Cerrar_Click);
			// 
			// InPrin
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(800, 290);
			this.Controls.Add(this.panel1);
			this.Name = "InPrin";
			this.Text = "InPrin";
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button BtCategoria;
		private System.Windows.Forms.Button BtClientes;
		private System.Windows.Forms.Button AdminUser;
		private System.Windows.Forms.Button Cerrar;
	}
}