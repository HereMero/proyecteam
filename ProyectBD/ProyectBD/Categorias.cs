﻿using ProyectBD.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectBD
{
	public partial class Categorias : Form
	{
		public Categorias()
		{
			InitializeComponent();
		}

		Proyect1Entities BD = new Proyect1Entities();
		private void btnNuevo_Click(object sender, EventArgs e)
		{
			Limpiar();
		}


		void Limpiar()
		{

			TxClave.Enabled = true;
			TxDescripcion.Text = "";
			TxTipo.Text = "";
			TxClave.Focus();
		}

		private void Grid_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			if (e.RowIndex > -1)
			{
				CargarCategoria(getCategoria(Grid[0, e.RowIndex].Value.ToString()));
			}
		}


		Categoria getCategoria(string Clave)
		{
			Categoria categoria = BD.Categoria.Find(Clave);

			return categoria;
		}

		void CargarCategoria(Categoria categoria)
		{
			TxClave.Text = categoria.Cat_Clave.ToUpper();
			TxDescripcion.Text = categoria.Cat_Descripción.ToUpper();
			TxTipo.Text = categoria.Cat_Tipo.ToUpper();
			//cfFecha.Value = cliente.Cli_FechaNac.Value;
			TxClave.Enabled = false;

		}

		private void Grid_CellContentClick(object sender, DataGridViewCellEventArgs e)
		{

		}

		private void btnGuardar_Click(object sender, EventArgs e)
		{
			if (TxClave.Text.Trim() == "")
			{
				MessageBox.Show("llena la clave que es por favor");
				TxClave.Focus();
				return;
			}

			if (TxDescripcion.Text.Trim() == "")
			{
				MessageBox.Show("llena la descripcion");
			     TxDescripcion.Focus();
				return;
			}
			if (TxTipo.Text.Trim() =="")
			{
				MessageBox.Show("llena el tipo");
				TxTipo.Focus();
				return;
			}
			Categoria categoria;
			if (TxClave.Enabled)
			{
				categoria = new Categoria();
				categoria.Cat_Clave =TxClave.Text.ToUpper();
				categoria.Cat_Descripción = TxDescripcion.Text.ToUpper();
				categoria.Cat_Tipo = TxTipo.Text.ToUpper();
			
				BD.Categoria.Add(categoria);
			}
			else
			{
				categoria = getCategoria(TxClave.Text);
				if (categoria != null)
				{
					categoria.Cat_Descripción = TxDescripcion.Text.ToUpper();
				}

			}
			BD.SaveChanges();
			TxClave.Enabled = false;
			MessageBox.Show("te la rifaste carnal se guardo");
			LlenarGrid();
		}
		void LlenarGrid()
		{
			Grid.DataSource = BD.Categoria.ToList();
		}

		private void btnEliminar_Click(object sender, EventArgs e)
		{
			if (!TxClave.Enabled)
			{
				if (MessageBox.Show("Desea eliminar la categoria", "Eliminar", MessageBoxButtons.YesNo,
					MessageBoxIcon.Question) == DialogResult.Yes)
				{
					Categoria cli = getCategoria(TxClave.Text);
					BD.Categoria.Remove(cli);
					BD.SaveChanges();
					MessageBox.Show("categoria: " + TxClave.Text + "eliminado exitosamente!");
					LlenarGrid();
					Limpiar();

				}
			}
		}

		private void Categorias_Load(object sender, EventArgs e)
		{
			// TODO: esta línea de código carga datos en la tabla 'proyect1DataSet.Categoria' Puede moverla o quitarla según sea necesario.
			this.categoriaTableAdapter.Fill(this.proyect1DataSet.Categoria);

		}
	}
}
